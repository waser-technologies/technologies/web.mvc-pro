FROM python:3.6-stretch

COPY web_mvc_pro web_mvc_pro/

WORKDIR /web_mvc_pro

RUN pip install -r requirements/prod.txt
RUN pip install -r requirements/git_prod.txt

EXPOSE 8000
EXPOSE 5432

COPY ./entrypoint.sh /entrypoint.sh

RUN chmod +x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]

CMD ["gunicorn", "--log-file=/var/log/gunicorn.log", "Web_mvc.wsgi", "-b 0.0.0.0:8000"]
