# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url, include

from shop.views.catalog import AddToCartView
from commerce.serializers import AddWearableModelToCartSerializer


urlpatterns = [    
    url(r'^(?P<slug>[\w-]+)/add-wearable-to-cart/', AddToCartView.as_view(
        serializer_class=AddWearableModelToCartSerializer,
    )),
]
