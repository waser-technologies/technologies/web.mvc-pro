# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.forms.widgets import Select
from django.utils.translation import ugettext_lazy as _
from django_filters import FilterSet

from djng.forms import NgModelFormMixin
from djng.styling.bootstrap3.forms import Bootstrap3Form

from shop.filters import ModelChoiceFilter

from commerce.models import Brand, Product


class FilterForm(NgModelFormMixin, Bootstrap3Form):
    scope_prefix = 'filters'


class BrandFilterSet(FilterSet):
    Brand = ModelChoiceFilter(
        queryset=Brand.objects.all(),
        widget=Select(attrs={'ng-change': 'filterChanged()'}),
        empty_label=_("Any Brand"),
        help_text=_("Restrict product on this Brand only"),
    )

    class Meta:
        model = Product
        form = FilterForm
        fields = ['Brand']

    @classmethod
    def get_render_context(cls, request, queryset):
        # create filter set with bound form, to enable the selected option
        filter_set = cls(data=request.GET)

        # we only want to show Brands for products available in the current list view
        filter_field = filter_set.filters['Brand'].field
        filter_field.queryset = filter_field.queryset.filter(
            id__in=queryset.values_list('brand_id'))
        return dict(filter_set=filter_set)
