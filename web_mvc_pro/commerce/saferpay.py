import requests

class SaferPayAPI(object):
    
    def new_header():
        header = {}
        header['Content-Type'] = "application/json; charset=utf-8"
        header['Accept'] = "application/json"
        return header