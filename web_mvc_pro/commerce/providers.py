from shop.models.order import BaseOrder, OrderPayment, OrderModel
from shop.money import MoneyMaker
from shop.payment.providers import PaymentProvider
from django_fsm import transition
import requests
import base64
import hashlib
from django.utils.translation import gettext as _
from django.conf import settings


class SaferPayPayment(PaymentProvider):
    """
    Provides a simple prepayment payment provider.
    """
    namespace = 'safer-pay-payment'

    def __init__(self):
        if (not callable(getattr(OrderModel, 'no_payment_required', None)) or
            not callable(getattr(OrderModel, 'awaiting_payment', None))):
            msg = "Missing methods in Order model. Add 'shop.payment.workflows.ManualPaymentWorkflowMixin' to SHOP_ORDER_WORKFLOWS."
            raise ImproperlyConfigured(msg)
        super(SaferPayPayment, self).__init__()

    def get_payment_request(self, cart, request):
        order = OrderModel.objects.create_from_cart(cart, request)
        order.populate_from_cart(cart, request)
        if order.total == 0:
            order.no_payment_required()
        else:
            order.awaiting_payment()
        order.save(with_notification=True)
        return 'window.location.href="{}";'.format(order.get_absolute_url())

class OrderWorkflowMixin(object):
    TRANSITION_TARGETS = {
        'paid_with_saferpay': _("Paid using SaferPay"),
    }

    def __init__(self, *args, **kwargs):
        if not isinstance(self, BaseOrder):
            raise ImproperlyConfigured("class 'OrderWorkflowMixin' is not of type 'BaseOrder'")

        super(OrderWorkflowMixin, self).__init__(*args, **kwargs)

    @transition(field='status', source=['created'], target='paid_with_saferpay')
    def add_saferpay_payment(self, charge):
        assert self.currency == charge['currency'].upper(), "Currency mismatch"
        Money = MoneyMaker(self.currency)
        amount = Money(charge['amount']) / Money.subunits
        OrderPayment.objects.create(
            order=self,
            amount=amount,
            transaction_id=charge['id'],
            payment_method=SaferPayPayment.namespace,
        )

    def is_fully_paid(self):
        return super(OrderWorkflowMixin, self).is_fully_paid()

    @transition(field='status', source='paid_with_saferpay', conditions=[is_fully_paid],
                custom=dict(admin=True, button_name=_("Acknowledge Payment")))
    def acknowledge_saferpay_payment(self):
        self.acknowledge_payment()

    def refund_payment(self):
        """
        Refund the payment using SaferPay's refunding API.
        """
        Money = MoneyMaker(self.currency)
        filter_kwargs = {
            'transaction_id__startswith': 'ch_',
            'payment_method': SaferPayPayment.namespace,
        }
        for payment in self.orderpayment_set.filter(**filter_kwargs):
            refund = stripe.Refund.create(charge=payment.transaction_id)
            if refund.status_code == 200 and refund['ResponseHeader']['RequestId'] == request_id and refund['Transaction']['Status'] == "AUTHORIZED":
                amount = Money(refund['Transaction']['Amount']['Value']) / Money.subunits
                OrderPayment.objects.create(
                    order=self,
                    amount=-amount,
                    transaction_id=refund['Transaction']['Id'],
                    payment_method=SaferPayPayment.namespace,
                )

        del self.amount_paid  # to invalidate the cache
        if self.amount_paid:
            # proceed with other payment service providers
            super(OrderWorkflowMixin, self).refund_payment()

    def cancelable(self):
        return super(OrderWorkflowMixin, self).cancelable() or self.status in ['paid_with_saferpay']