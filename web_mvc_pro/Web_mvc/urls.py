# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function, unicode_literals
import os

from cms.sitemaps import CMSSitemap
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.contrib.sitemaps.views import sitemap
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.static import serve

admin.autodiscover()

urlpatterns = [
    url(r'^sitemap\.xml$', sitemap,{'sitemaps': {'cmspages': CMSSitemap}}),
    url(r'^taggit_autosuggest/', include('taggit_autosuggest.urls')),
]

if settings.USE_I18N:
    urlpatterns += i18n_patterns(
        url(r'^admin/', include(admin.site.urls)),
    )
else:
    urlpatterns += [
        url(r'^admin/', include(admin.site.urls)),
    ]

if settings.DS_SETTINGS != None:
    for urlpattern in settings.DS_SETTINGS.get('urlpatterns', []):
        if urlpattern['include'].get('namespace', False) and urlpattern['include'].get('app_name', False):
            urlpatterns.append(url(urlpattern.get('url'), include(urlpattern['include'].get('module'), namespace=urlpattern['include'].get('namespace'), app_name=urlpattern['include'].get('app_name'))))
        if urlpattern['include'].get('namespace', False):
            urlpatterns.append(url(urlpattern.get('url'), include(urlpattern['include'].get('module'), namespace=urlpattern['include'].get('namespace'))))
        if urlpattern['include'].get('app_name', False):
            urlpatterns.append(url(urlpattern.get('url'), include(urlpattern['include'].get('module'), app_name=urlpattern['include'].get('app_name'))))
        else:
            urlpatterns.append(url(urlpattern.get('url'), include(urlpattern['include'].get('module'))))

urlpatterns += [
        url(r'^', include('djangocms_forms.urls')),
        url(r'^', include('cms.urls')),
    ]

# This is only needed when using runserver.
if os.environ.get('DEV', False):
    urlpatterns = [
        url(r'^media/(?P<path>.*)$', serve,
            {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
        ] + staticfiles_urlpatterns() + urlpatterns
