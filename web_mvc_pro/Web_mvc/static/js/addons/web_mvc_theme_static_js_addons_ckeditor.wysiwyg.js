// #############################################################################
// CKEDITOR
/**
 * Default CKEDITOR Styles
 * Added within theme/settings.py CKEDITOR_SETTINGS.stylesSet
 *
 * @module CKEDITOR
 */
/* global CKEDITOR */

CKEDITOR.allElements = ['p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'div'];
CKEDITOR.stylesSet.add('default', [
    /* Themes Styles */
    {name: 'Feature title', element: 'h1', attributes: {class: 'display-4 text-white'} },
    {name: 'Section title', element: 'h3', attributes: {class: 'section-title m-5'} },
    {name: 'Footer title', element: 'h4', attributes: {class: 'text-white'} },
    {name: 'Small', element: 'small'},
    {name: 'Badge Green', element: 'span', attributes: {class: 'badge badge-success'} },
    {name: 'Code', element: 'code'},
    {name: 'Code block', element: 'pre'},
    {name: 'Scrollable code block', element: 'pre'},
    {name: 'Variable', element: 'var'},
    {name: 'User Input', element: 'kbd'},
    {name: 'Sample Output', element: 'samp'},
]);

/*
 * Extend ckeditor default settings
 * DOCS: http://docs.ckeditor.com/#!/api/CKEDITOR.dtd
 */
CKEDITOR.dtd.$removeEmpty.span = 0;
CKEDITOR.dtd.$removeEmpty.i = 0;