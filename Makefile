include ./.env

env:
	pip install -r './web_mvc_pro/requirements/base.txt'
	pip install -r './web_mvc_pro/requirements/git.txt'

clone:	
	git clone $(GIT_PATH) .

pull:
	git pull $(GIT_PATH)

image:
	docker build -t $(DOCKER_IMAGE) .
	docker login -u $(DOCKER_USER) -p $(DOCKER_PASSWORD)
	docker push $(DOCKER_IMAGE)

static:
	python ./web_mvc_pro/manage.py collectstatic --no-input

init:
	python ./web_mvc_pro/manage.py migrate --no-input
	python ./web_mvc_pro/manage.py collectstatic --no-input
	python ./web_mvc_pro/manage.py createsuperuser

migrations:
	python ./web_mvc_pro/manage.py makemigrations --no-input

migrate:
	python ./web_mvc_pro/manage.py migrate --no-input

server:
	python ./web_mvc_pro/manage.py runserver